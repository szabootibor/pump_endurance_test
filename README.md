# Run an electrical pump test program and read sensor data
This app allows the configuration of a pump hardware test program, that repeatedly pumps a vaccuum into a glas bowl.
A set of different sensors values is read and are published within the device swarm. 
You can create a Datapod and therein a Swarm-Source to receive the published data in near realtime from the device's swarm. 

It also observes system failures and sends an email with the failure conditions to a list of recipients.

<img src="https://cdn-5e5150f5f911c807c41ebdc8.closte.com/wp-content/uploads/Prototyp-mit-Bildschirm-2.jpeg">

See [__here__](https://www.record-evolution.de/en/blog/iot-use-case-with-an-electric-vacuum-pump/) for a full explanation of this use-case and the edge application.

# Parameters

Parameter | Description
---|---
Measurement Frequency | How many Measurements should be done per Second
Motor Speed | Input the speed of the motor in RPM
Vacuum Pressure Low (Pump stops) | Pressure when the pump stops
Vacuum Pressure High (Pump starts) | Pressure when the pump starts
Vacuum Pressure Timeout | Maximum time in seconds the pressure may stay above the maximum
Vacuum Keep Timeout | Time the vacuum is kept after reaching the maximum and before releasing the pressure
Email for Notifications | Email address to use for alert notifications
Mailgun Key | Secret Access Key for the Mailgun Mail Service that is required to send emails
Max Current | At this current the app stops the pump and sends an email
Max Voltage | At this voltage the app stops the pump and sends an email
Max Temperature | At this temperature the app stops the pump and sends an email

# LICENSE
### MIT
Copyright (c) 2021 Record Evolution GmbH
See license file on the source code