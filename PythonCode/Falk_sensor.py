import datetime
import time
import os # verschicken von Daten
from autobahn.twisted.component import Component, run
from autobahn.twisted.util import sleep
from twisted.internet.defer import inlineCallbacks, Deferred, CancelledError

from twisted.internet.task import react, LoopingCall
from twisted.internet import reactor

from smbus2 import SMBus, i2c_msg
from SmcG2I2C import SmcG2I2C
import Adafruit_ADS1x15

import pressure_AMS5812
import MCP9600
import RPMMeter

import RPi.GPIO as GPIO

from Switch import *
from Button import *

from Configuration import *
from MeasurementWaiter import *

from Email import send_notify_email

global motor_controller
motor_controller = None
main_loop = None
global sess
sess = None

from queue import SimpleQueue

#################################################
# Main program
#################################################

def ads1115_voltage(channel, gain):
    adc = Adafruit_ADS1x15.ADS1115(address=ADS1115_ADDRESS, busnum=I2C_BUS)
    return adc.read_adc(channel, gain)*6.144/32768

def log(msg):
    if LOG_VERBOSE:
        print(msg)


from enum import Enum

class StateMachine(object):
    class State(Enum):
        IDLE    = 0
        RUNNING = 1
        ERROR   = 2 

    def __init__(self, state=State.IDLE):
        self.state = state
    
    def button_pressed(self, memo):
        global main_loop
        global sess
        if self.state == self.State.IDLE:
            main_loop = mainLoop()
            self.state = self.state.RUNNING
            pubEvent('button', {'old_state': 'IDLE', 'new_state': 'RUNNING'})
        elif self.state == self.State.RUNNING:
            log("Stopping")
            main_loop.cancel()
            main_loop = None
            motor_controller.set_target_speed(0)
            vacuum_valve.switch(OFF)
            pubEvent('valve', {'new_state': 'OFF'})
            self.state = self.State.IDLE
            pubEvent('button', {'old_state': 'RUNNING', 'new_state': 'IDLE'})
        elif self.state == self.State.ERROR:
            if emergency_limits_violated(measurement_data):
                log("Emergency limits still violated; do not clear error")
                pubEvent('button', {'old_state': 'ERROR', 'new_state': 'ERROR'})
            else:
                error_lamp.switch(OFF)
                pubEvent('error_lamp', {'new_state': 'OFF'})
                self.state = self.state.IDLE
                pubEvent('button', {'old_state': 'ERROR', 'new_state': 'IDLE'})
        else:
            log("Invalid state!")
            sys.exit(1)
            
    def error(self, message=""):
        global main_loop
        log("Error happened")

        if main_loop is not None:
            log("cancelling main_loop")
            main_loop.cancel()
            main_loop = None
        
        if (self.state != self.State.ERROR):
            motor_controller.set_target_speed(0)
            vacuum_valve.switch(OFF)
            pubEvent('valve', {'new_state': 'OFF'})
            error_lamp.switch(ON)
            pubEvent('error_lamp', {'new_state': 'ON'})
            # transition to error state: Send e-mail
            log("Sending e-mail")
            send_notify_email(message, measurement_data)
            self.state = self.State.ERROR

def emergency_limits_violated(measurement_data):
    for key in measurement_data:
        if measurement_data[key]['violated']:
            return True
    return False

def clear_emergency_limits_violation(measurement_data):
    rv = False
    for key in measurement_data:
        if measurement_data[key]['violated']:
            rv = True
            measurement_data[key]['violated'] = False
    return rv

def cyclic_fetch_sensors(measurement_data, measurement_waiter):
    global motor_controller
    pt = ams5812.pressure_temperature()
    # log(f"AMS5812 Druck: {pt['pressure']:.2f} mbar Temperatur: {pt['temperature']:.2f} °C")
    temp = mcp9600.temperature()
    # log(f"MCP9600 Temperatur: {temp:.2f} °C")

    
    v_12v_div3 = ads1115_voltage(channel=1, gain=2/3) # Use 2/3 gain to have the required voltage limit
    # Voltage is divided by resistor net by 3 so multiply it by 3 to get real voltage
    v_12v = v_12v_div3 * 3
    # log(f"Versorgungsspannung (12v):  {v_12v:.2f} V")

    v_current_transducer = ads1115_voltage(channel=0, gain=2/3)
    voffset = 2.5
    current = (v_current_transducer - voffset) * 1000.0 / 25.0 # sensitivity: 25 mV/A
    # log(f"Stromwandler Ausgansspannung:  {v_current_transducer:.2f} V, errechneter Strom: {current:.2f} A")
    tsp = datetime.datetime.now().strftime('%Y-%m-%dT%H:%M:%S.%f%z')
    measurement_data['timestamp']['value'] = tsp
    measurement_data['pressure']['value'] = pt['pressure']
    measurement_data['env_temperature']['value'] = pt['temperature']
    measurement_data['motor_temperature']['value'] = temp
    measurement_data['supply_voltage']['value'] = v_12v
    measurement_data['motor_current']['value'] = current

    measurement_queue.put([tsp, pt['pressure'], pt['temperature'], temp, v_12v, current, rpm_meter.rpm()])

    # Now check emergency limits
    if temp > EMERGENCY_TEMPERATURE_MAX:
        measurement_data['motor_temperature']['violated'] = True
    
    if v_12v < EMERGENCY_VOLTAGE_MIN:
        measurement_data['supply_voltage']['violated'] = True

    if current > EMERGENCY_CURRENT_MAX:
        measurement_data['motor_current']['violated'] = True

    if (emergency_limits_violated(measurement_data)):
        print("!!!!!!!!!!!!!!!!!!!!! Emergency limit violated !!!!!!!!!!!!!!!!!!!", measurement_data)
        state_machine.error("Emergency limit violated")
    else:
        if clear_emergency_limits_violation(measurement_data):
            log("Emergency condition cleared")

    measurement_waiter.measurement_ready(measurement_data)

      
def publishData(session):
    # Zusätzliche Messdaten aufnehmen!
    # session.publish(TOPIC, {"time": str(datetime.datetime.now()), "pressure": measurement_data["pressure"],
        # "temperature": measurement_data["env_temperature"]})
    log(f"Publishing data. Snapshot: {measurement_data}")
    payload = []
    while not measurement_queue.empty():
        payload.append(measurement_queue.get())
    session.publish(TOPIC, payload, device=os.getenv('DEVICE_NAME'))

def pubEvent(name, payload):
    global sess
    if sess:
        payload.update({'name': name, 'timestamp': datetime.datetime.now().strftime('%Y-%m-%dT%H:%M:%S.%f%z')})
        log(f"publishing control {payload}")
        sess.publish(TOPIC + '.control', payload , device=os.getenv('DEVICE_NAME'))


component = Component(transports=URL, realm=REALM, authentication={"wampcra": {"authid":AUTHID, "authrole":"user", "secret":SECRET}})

@component.on_join
@inlineCallbacks
def joined(session, details):
    log("Publisher running")
    global sess
    sess = session
    publishLoop = LoopingCall(publishData, session=session)
    publishLoopDeferred = publishLoop.start(PUBLISH_DATA_PERIOD_SECOND)
    yield publishLoopDeferred


def logRPM():
    log(f"RPM: {int(rpm_meter.rpm())}")

@inlineCallbacks
def mainLoop():
    global motor_controller, state_machine

    log("enter mainLoop")
    
    try:
        # first wait until measurement is active
        yield mw.await_measurement(lambda m: True)
        while True: # Endlosschleife
            vacuum_valve.switch(ON)
            pubEvent('valve', {'new_state': 'ON'})
            log(f"Setting motorctr target speed auf {MOTORCTRL_TARGET_SPEED}")
            motor_controller.set_target_speed(MOTORCTRL_TARGET_SPEED)
            pubEvent('motor', {'target_speed': MOTORCTRL_TARGET_SPEED})

            try:
                yield mw.await_measurement(lambda m: m['pressure']['value'] < VACUUM_TARGET_PRESSURE_LOW, timeout=AWAIT_TARGET_PRESSURE_TIMEOUT)
            except AwaitTimeout:
                log(f"Low pressure not reached within {AWAIT_TARGET_PRESSURE_TIMEOUT} seconds")
                state_machine.error(f"Low pressure not reached within {AWAIT_TARGET_PRESSURE_TIMEOUT} seconds")
                motor_controller.set_target_speed(0)
                pubEvent('motor', {'target_speed': 0})
                vacuum_valve.switch(OFF)
                pubEvent('valve', {'new_state': 'OFF'})
                break
            
            motor_controller.set_target_speed(0)
            pubEvent('motor', {'target_speed': 0})

            if measurement_data["pressure"]['value'] > -10:
                log(f"Pressure under limit: {measurement_data['pressure']['value']}")
                state_machine.error(f"Pressure under limit: {measurement_data['pressure']['value']}")
            else:
                log(f"Waiting {KEEP_VACUUM_SECONDS} before draining vacuum")
                # Wait asynchronously: autobahn.twisted.util.sleep
                yield sleep(KEEP_VACUUM_SECONDS)
                # Vakuum ablassen
                vacuum_valve.switch(OFF)
                pubEvent('valve', {'new_state': 'OFF'})
                try:
                    yield mw.await_measurement(lambda m: m['pressure']['value'] > VACUUM_TARGET_PRESSURE_HIGH, timeout=AWAIT_TARGET_PRESSURE_TIMEOUT)
                except AwaitTimeout:
                    log(f"High pressure not reached within {AWAIT_TARGET_PRESSURE_TIMEOUT} seconds")
                    state_machine.error(f"High pressure not reached within {AWAIT_TARGET_PRESSURE_TIMEOUT} seconds")

    except CancelledError as err:
        log("Main loop cancelled")
        mw.cancel()

    log("exit mainLoop")

if __name__ == "__main__":

    try :
        bus = SMBus(I2C_BUS)

        mcp9600 = MCP9600.MCP9600(bus, ADDRESS_MCP9600)
        mcp9600.cfg()
        
        ams5812 = pressure_AMS5812.AMS5812(bus, ADDRESS_AMS5812)
        ams5812.cfg()
        
        error_lamp = Lamp(ERROR_LAMP_PORT)
        app_running_lamp = Lamp(APP_RUNNING_LAMP_PORT)
        app_running_lamp.switch(ON)
        pubEvent('running_lamp', {'new_state': 'ON'})
        
        vacuum_valve = VacuumValve(VACUUM_VALVE_PORT)

        # Starting motor controller
        motor_controller = SmcG2I2C(bus, ADDRESS_MOTORCTRL)
        motor_controller.exit_safe_start()
        measurement_queue = SimpleQueue()
                
        rpm_meter = RPMMeter.RPMMeter(RPM_PORT, dt=RPM_METER_DT, alpha=RPM_METER_ALPHA)
        rpm_meter.cfg()
        rpmLoop = LoopingCall(logRPM)
        rpmLoopDeferred = rpmLoop.start(RPM_DISPLAY_PERIOD_SECOND)
        
        mw = MeasurementWaiter()
        measurement_data = { #'emergency_limits_violated':False,
            'timestamp': {'name': 'Zeitpunkt', 'unit': "UTC",  'value': None, 'violated':False},
            'motor_temperature': {'name': 'Motortemperatur', 'unit': "°C",  'value': 0, 'violated':False},
            'env_temperature': {'name': 'Umgebungstemperatur', 'unit': "°C", 'value': 0, 'violated':False},
            'supply_voltage': {'name': 'Versorgungsspannung', 'unit': "V", 'value': 0, 'violated':False},
            'motor_current': {'name': 'Pumpenmotorstrom', 'unit': "A", 'value': 0, 'violated':False},
            'pressure': {'name': 'Vakuum', 'unit': "mbar", 'value': 0, 'violated': False},
        }


        state_machine = StateMachine(StateMachine.State.RUNNING)
        
        measurementLoop = LoopingCall(cyclic_fetch_sensors, measurement_data=measurement_data, measurement_waiter = mw )
        measurementLoopDeferred = measurementLoop.start(SENSOR_FETCH_PERIOD_SECOND)

    
        main_loop = mainLoop()
        
        button = Button(BUTTON_PORT, state_machine.button_pressed, 0)

        #reactor.run()
        print('starting wamp component now ...')
        run([component])
    except Exception as e:
        print('Failed to Start ' + str(e))
    
    finally:
        log("Graceful shutdown")
        motor_controller.set_target_speed(0)
        # pubEvent('motor', {'target_speed': 0})
        vacuum_valve.switch(OFF)
        # pubEvent('valve', {'new_state': 'OFF'})
        error_lamp.switch(OFF)
        # pubEvent('error_lamp', {'new_state': 'OFF'})
        app_running_lamp.switch(OFF)
        # pubEvent('running_lamp', {'new_state': 'OFF'})
        GPIO.cleanup()

