from twisted.internet.defer import Deferred
from twisted.internet import reactor

class AwaitTimeout(Exception):
    pass
        
class MeasurementWaiter(object):
    def __init__(self):
        self.triggers = set()

    class Trigger(object):
        def __init__(self, mw, expr, timeout):
            self.expr = expr
            self.d = Deferred()
            if timeout:
                self.timeout = reactor.callLater(timeout, mw.timeout, trigger=self)
            else:
                self.timeout = None

        def cancel_timeout(self):
            if self.timeout is not None:
                self.timeout.cancel()

    def timeout(self, trigger):
        self.triggers.remove(trigger)
        trigger.d.errback(AwaitTimeout())
    
    def measurement_ready(self, mdata):
        ready = set()
        for trigger in self.triggers:
            if trigger.expr(mdata):
                ready.add(trigger)

        for trigger in ready:
            trigger.d.callback(result=True)
            self.triggers.remove(trigger)
            trigger.cancel_timeout()
    
    def await_measurement(self, expr, timeout=0):
        trigger = self.Trigger(self, expr, timeout)
        self.triggers.add(trigger)
        return trigger.d

    def cancel(self):
        for trigger in self.triggers:
            trigger.cancel_timeout()
        self.triggers.clear()
