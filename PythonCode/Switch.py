import RPi.GPIO as GPIO
import datetime
import os

ON = True
OFF = False

class Switch(object):
    def __init__(self, port):
        self.port = port
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(port, GPIO.OUT)
        self.switch(OFF)
        
    def switch(self, onoff):
        GPIO.setmode(GPIO.BCM)
        GPIO.output(self.port, GPIO.HIGH if onoff else GPIO.LOW)
        

class Lamp(Switch):
    pass

class MotorRelay(Switch):
    pass

class VacuumValve(Switch):
    pass
