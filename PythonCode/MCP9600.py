
# MCP9600 temperature sensor object

class MCP9600(object):
  def __init__(self, bus, address):
    self.bus = bus
    self.address = address

  def cfg(self):
    config = b'\x05\x00'
    self.bus.write_i2c_block_data(self.address, 0, config)
    config = b'\x06\x00'
    self.bus.write_i2c_block_data(self.address, 0, config)

  def temperature(self):
    data = self.bus.read_i2c_block_data(self.address, 0, 2)
    if data[0] & 0x80:
        r = data[0] * 16 + data[1] / 16 - 4096
    else:
        r = data[0] * 16 + data[1] * 0.0625
    return r
