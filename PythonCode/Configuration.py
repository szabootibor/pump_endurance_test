import os

#################################################
# GPIO Channels Configuration
#################################################

#MOTOR_RELAY_PORT = 4
VACUUM_VALVE_PORT = 20
ERROR_LAMP_PORT = 23
APP_RUNNING_LAMP_PORT = 16
RPM_PORT = 26
BUTTON_PORT=19


#################################################
# I2C Channels Configuration
#################################################

I2C_BUS = 1
ADDRESS_MOTORCTRL = 13
ADDRESS_MCP9600 = 0x63
ADDRESS_AMS5812 = 0x78
ADS1115_ADDRESS = 0x48

#################################################
# RPM Meter configuration
#################################################
RPM_METER_DT = 0.25 # sampling period dt (seconds)
RPM_METER_ALPHA = 0.7 # alpha for exponential moving average
                      # greater alpha means faster output change (less smoothing effect)

#################################################
# Motor controller configuration
#################################################
# Target speed von 0..-3200
# Speed absolute values under 500 may not start the motor!
# MOTORCTRL_TARGET_SPEED = -1000
MOTORCTRL_TARGET_SPEED = -int(os.getenv('MOTORCTRL_TARGET_SPEED', 1500))

#################################################
# Timing configuration
#################################################

RPM_DISPLAY_PERIOD_SECOND = 2
PUBLISH_DATA_PERIOD_SECOND = 1
SENSOR_FETCH_PERIOD_SECOND = 1/float(os.getenv('SENSOR_FETCH_HERTZ', 10))

#################################################
# Emergency configuration
#################################################

EMERGENCY_TEMPERATURE_MAX = float(os.getenv('EMERGENCY_TEMPERATURE_MAX', 50))
EMERGENCY_VOLTAGE_MIN = float(os.getenv('EMERGENCY_VOLTAGE_MIN', 10))
EMERGENCY_CURRENT_MAX = float(os.getenv('EMERGENCY_CURRENT_MAX', 50))

#################################################
# Vacuum pump loop configuration
#################################################
VACUUM_TARGET_PRESSURE_LOW = int(os.getenv('VACUUM_TARGET_PRESSURE_LOW', -400))
VACUUM_TARGET_PRESSURE_HIGH = int(os.getenv('VACUUM_TARGET_PRESSURE_HIGH', -100))
AWAIT_TARGET_PRESSURE_TIMEOUT = float(os.getenv('AWAIT_TARGET_PRESSURE_TIMEOUT', 10.0))
KEEP_VACUUM_SECONDS = float(os.getenv('KEEP_VACUUM_SECONDS', 3.0))

#################################################
# E-mail notification configuration
#################################################
MAILGUN_KEY = os.getenv('MAILGUN_KEY')
EMAIL_NOTIFICATION_TO = os.getenv('EMAIL_NOTIFICATION_TO')

#################################################
# Message broker configuration
#################################################

URL = os.environ.get('CBURL', u'wss://cbw.record-evolution.com/ws-ua-usr')
REALM = os.environ.get('CBREALM', u'userapps')
TOPIC = os.environ.get('CBTOPIC', u'example.pumpe.data') # auf dem Kanal "example.pumpe.data" kannst du dann im Datapod mit eine IoT-Roh-Tabelle lauschen.
# authid = "b9f7b4cd-865d-4ba4-a5d1-826881d081f9"
AUTHID_DEFAULT = "44cf84a3-0bc6-4913-9c01-77f7f7ede90d"
AUTHID = os.environ.get('DEVICE_SERIAL_NUMBER', AUTHID_DEFAULT) # This ENV variable always contains the present devices serial number
SECRET = AUTHID

#################################################
# Logging
#################################################
LOG_VERBOSE = True

