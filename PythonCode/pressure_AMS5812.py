# AMS5812 pressure and temperature sensor object

from smbus2 import i2c_msg

class AMS5812(object):
    def __init__(self, bus, address):
        self.bus = bus
        self.address = address

    def cfg(self):
        pass
        
    def pressure_temperature(self):
        msg = i2c_msg.read(self.address, 4)
        self.bus.i2c_rdwr(msg)

        data = list(msg)

        pressure_mres = ((data[0] << 8) + data[1]) # Byte 1 + Byte 2

        pressure_offset = 3277   # read pressure value if measured value = 0 millibar / see Datasheet
        pressure_range = 26214   # count of pressure measurement steps / see Datasheet
        pressure_maxval = 2068.0 # maximal measured pressure in millibar / see Datasheet

        pressure_step = pressure_maxval / pressure_range

        pressure_mbar = ( pressure_mres - pressure_offset ) * pressure_step
        underpressure_mbar = - pressure_mbar

        temp = ((data[2] << 8) + data[3]) # Byte 2 + Byte 3 is Temperature 
    
        # pressure = ((pressure - 3277.0) / ((26214.0) / 30.0)) - 30.0; / sample code of ncd.io
        cTemp = ((temp - 3277.0) / ((26214.0) / 110.0)) - 25.0 # / sample code of ncd.io
        # fTemp = (cTemp * 1.8 ) + 32 / sample code of ncd.io

        return { "pressure":underpressure_mbar, "temperature":cTemp }
