import RPi.GPIO as GPIO
from twisted.internet.task import LoopingCall
from twisted.internet import reactor
import time

class RPMMeter(object):
    def __init__(self, port, dt = 1.0, alpha = 0.8):
        self.port = port
        self.dt = dt
        self.alpha = alpha
        self.rpdt = 0
        self.counter = 0
        #self.edge_time = 0

    def count(self, channel):
        self.counter = self.counter + 1
        #edge_time = time.monotonic()
        #print(f"E: {int((edge_time - self.edge_time)*1000):d}")
        #self.edge_time = edge_time
        
    def on_edge(self, channel):
        reactor.callFromThread(self.count, channel)

    def sample(self):
        self.rpdt = self.alpha * self.counter + (1-self.alpha) * self.rpdt
        self.counter = 0
        
    def cfg(self):
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self.port, GPIO.IN)
        GPIO.add_event_detect(self.port, GPIO.FALLING, callback=self.on_edge)
        sampleLoop = LoopingCall(self.sample)
        sampleLoop.start(self.dt)

    def rpm(self):
        return self.rpdt * 60.0 / self.dt
