import RPi.GPIO as GPIO
from twisted.internet import reactor
from autobahn.twisted.util import sleep
from twisted.internet.defer import inlineCallbacks
from threading import Lock

class Button(object):
    def __init__(self, port, on_pressed, memo):
        self.port = port
        self.on_pressed = on_pressed
        self.memo = memo
        self.sharp = True
        self.lock = Lock()
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(port, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        GPIO.add_event_detect(self.port, GPIO.FALLING, callback=self.pressed)
        
    @inlineCallbacks
    def pressed_thread_safe(self):
        bounce_cycles = 19
        bounce_time = 0.005 # 5 ms

        cnt = 0
        for i in range(bounce_cycles):
            yield sleep(bounce_time)
            if not GPIO.input(self.port):
                cnt = cnt + 1

        if cnt > bounce_cycles/2:
            self.on_pressed(self.memo)

        with self.lock:
            self.sharp = True
        
    def pressed(self, channel):
        with self.lock:
            if not self.sharp:
                return
            self.sharp = False

        reactor.callFromThread(self.pressed_thread_safe)

