import requests
import os

from Configuration import *

def send_notify_email(message, data):
    if not MAILGUN_KEY:
        print('can not send emergency email. No MAILGUN_KEY provided in the parameters.')
        return False

    formated = format_notify_email(data)

    p_body = f"""
        <html><body>
        <h3> Emergency Limit violation </h3>

        <h4> Device: {os.getenv('DEVICE_NAME')}</h4>
        <p>
        {message}
        </p>
        {formated}
        </body></html>
        """

    requests.post(
        "https://api.mailgun.net/v3/mg.record-evolution.com/messages",
        auth=("api", MAILGUN_KEY),
        data={"from": "Pump App <info@record-evolution.com>",
            "to": EMAIL_NOTIFICATION_TO,
            #  "cc": "baz@example.com",
            #  "bcc": "info@record-evolution.de",
            "subject": f"Error in Device: {os.getenv('DEVICE_NAME')} ({message})",
            "text": message,
            "html": p_body
            }
        )

def format_notify_email(measurement_data):
    rv = "<h2>Data Snapshot: </h2>"
    for key in measurement_data:
        val = measurement_data[key]['value']
        try:
            val = "{:.2f}".format(val)
        except:
            pass
        if measurement_data[key]['violated']:
            rv += f"<p style=\"color:red\">{measurement_data[key]['name']}: {val} {measurement_data[key]['unit']}</p>"
        else:
            rv += f"<p>{measurement_data[key]['name']}: {val} {measurement_data[key]['unit']}</p>"
    return rv