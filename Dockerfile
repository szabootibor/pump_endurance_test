FROM python

WORKDIR /app

COPY requirements.txt /app

ENV CFLAGS=-fcommon

# Get Rust; NOTE: using sh for better compatibility with other base images
RUN curl https://sh.rustup.rs -sSf | sh -s -- -y

# Add .cargo/bin to PATH
ENV PATH="/root/.cargo/bin:${PATH}"

RUN pip3 install --upgrade pip

RUN pip3 install -r requirements.txt
COPY . /app
RUN pip3 install GPIO/


ENTRYPOINT [ "python3", "-u", "/app/PythonCode/Falk_sensor.py" ]
# ENTRYPOINT [ "sleep", "infinity" ]